import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FetchAllPokemonsResponse, Pokemon } from '../interfaces/pokemon.interface';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url: string = 'https://pokeapi.co/api/v2';



  constructor(private http: HttpClient) {}

  getPokemons(): Observable<Pokemon[]>{
    return this.http.get<FetchAllPokemonsResponse>(`${this.url}/pokemon?limit=1500`)
    .pipe(
      map( this.transformPokemon )
    )
  }

  private transformPokemon(resp : FetchAllPokemonsResponse): Pokemon[]{

    const pokemonList: Pokemon[] = resp.results.map(poke => {

      const urlArr = poke.url.split('/');
      const id = urlArr[6];
      const pic = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
      
      return {
        id: id,
        name: poke.name,
        pic: pic
      }
    });
    return pokemonList;
  }

}
