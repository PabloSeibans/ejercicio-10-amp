export interface FetchAllPokemonsResponse {
    count: number;
    next: null;
    previous: null,
    results: SmallPokemon[];
}

export interface Pokemon {
    id: string;
    name: string;
    pic: string;
}

export interface SmallPokemon {
    name: string;
    num: string;
    url: string;
}

