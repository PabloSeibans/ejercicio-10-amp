import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon.interface';
import { DataService } from 'src/app/services/data.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public pokemons: Pokemon[] = [];
  public page: number = 0;
  public buscar: string = '';


  constructor( private pokemonService: DataService) { }

  ngOnInit(): void {
    this.pokemonService.getPokemons().subscribe(pokemons => {
      this.pokemons = pokemons;
    });
  }

  AvPage(){
    this.page +=5;
  }
  AntPage(){
    if (this.page > 0) {
      this.page -=5; 
    }
  }

  BuscarPokemon(buscar: string){
    this.page = 0;
    this.buscar = buscar;
    
  }

}

