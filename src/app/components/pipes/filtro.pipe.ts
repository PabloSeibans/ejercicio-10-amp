import { Pipe, PipeTransform } from '@angular/core';
import { Pokemon } from 'src/app/interfaces/pokemon.interface';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(pokemons: Pokemon[], page: number = 0, buscar: string = ''): Pokemon[] {

    if (buscar.length ===0) {
      return pokemons.slice(page,page + 5);
    }
    const filterPokemon = pokemons.filter(poke => poke.name.includes(buscar));

    return filterPokemon.slice(page, page +5);
    
  }

}
